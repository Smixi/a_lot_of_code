# Brainfuck is a minimalist programming language consisting of 8 commands. That's all.
# However it is Turing complete and allows you to make whatever you want, if you are very patient and motivated.

# Your goal is to create a fully functional Brainfuck interpreter.
# Let see how it works.

# The Brainfuck model is composed of three elements:
# - An array of S one byte cells initialized to 0, and indexed from 0.
# - A pointer initialized to point to the first cell (index 0) of the array.
# - A program made up of the 8 valid instructions.

# The following are the instructions:
# - > increment the pointer position.
# - < decrement the pointer position.
# - + increment the value of the cell the pointer is pointing to.
# - - decrement the value of the cell the pointer is pointing to.
# - . output the value of the pointed cell, interpreting it as an ASCII value.
# - , accept a positive one byte integer as input and store it in the pointed cell.
# - [ jump to the instruction after the corresponding ] if the pointed cell's value is 0.
# - ] go back to the instruction after the corresponding [ if the pointed cell's value is different from 0.

# Note: The [ and ] commands always come in pairs, and in case of nested [] the first [ always correspond to the last ].

# Be careful: A Brainfuck program can contain any characters, that allow the developers to comment their code and to make it more readable. Of course your interpreter must ignore all of these "inactive" characters.

# In some cases, errors might be encountered. When this happens you have to stop the execution of the program and print the correct error message from the following list:
# - "SYNTAX ERROR" if a [ appears to have no ] to jump to, or vice versa. Note that this error must be raised before the execution of the program, no matter its position in the Brainfuck code.
# - "POINTER OUT OF BOUNDS" if the pointer position goes below 0 or above S - 1.
# - "INCORRECT VALUE" if after an operation the value of a cell becomes negative or higher than 255.

# UPDATE:
# The two following errors have been deleted:
# - "INVALID INPUT" if a given input can't be stored in a single byte.
# - "MISSING INPUT" if no input is provided when the program asks for one.
# The corresponding test cases and validators were suppressed, and the "Multiple errors" pair of test and validator was added instead.
# Entrée
# Line 1: Three integers L, S and I for the program line count, the needed array size and the inputs count.
# Next L lines: A line of the Brainfuck program.
# Next I lines: An integer input to the Brainfuck program.
# Sortie
# The output must be the characters sequence printed (.) by the Brainfuck program, or the correct error message if a problem is encountered.
# Note that the given programs will never try to use . on a a value corresponding to no printable ASCII characters.
# Contraintes
# 0 < L ≤ 100
# 0 ≤ I ≤ 100
# 0 < S ≤ 100
# Exemple
# Entrée
# 1 1 0
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++.+.+.
# Sortie
# ABC


import sys
import math
from enum import Enum
import re

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
class Instructions(Enum):
    P_INCREMENT = ">"
    P_DECREMENT = "<"
    C_INCREMENT = "+"
    C_DECREMENT = "-"
    C_OUTPUT = "."
    C_STORE = ","
    JUMP_START = "["
    JUMP_END = "]"

class BrainfuckErrors(Enum):
    INCORRECT_VALUE = "INCORRECT VALUE"
    P_OUT_OF_BOUND = "POINTER OUT OF BOUNDS"
    SYNTAX_ERROR = "SYNTAX ERROR"

instructions_set = set([i.value for i in Instructions])

lines, stack_size, inputs_number = [int(i) for i in input().split()]

program = "" 
integers =[]
pointer = 0
stack = [0 for i in range(stack_size)]

#Get program data
for line in range(lines):
    program += input()
#Get inputs for the program
inputs = [int(input()) for i in range(inputs_number)]
inputs_counter = 0
#print(program)
output = ""

#Redefine the program to remove comment and use enum/
program = [Instructions(c) for c in program if c in instructions_set]

labels = {}
jump_start_instruction = []
number_open = 0
#Check program syntax and labelize
for index, instruction in enumerate(program):
    if instruction == Instructions.JUMP_START:
        jump_start_instruction.append(index)
        #Prepare to where the instruction can jump:
        labels[index] = -1
        number_open += 1 
    if instruction == Instructions.JUMP_END:
        number_open -= 1
        if(number_open == -1):
            break
        pc_of_start = jump_start_instruction.pop()
        labels[index] = pc_of_start
        labels[pc_of_start] = index

#print(labels)
err = False
#Check number of opening and closing.
if number_open !=0:
    output = BrainfuckErrors.SYNTAX_ERROR.value
    err = True

#Execute program
if err == False:
    pc = -1
    while pc != len(program)-1:
        pc += 1
        instruction = program[pc]
        if instruction == Instructions.P_INCREMENT:
            #Check if OOB
            if pointer == stack_size-1:
                output = BrainfuckErrors.P_OUT_OF_BOUND.value
                break
            else:
                pointer += 1
            continue
        if instruction == Instructions.P_DECREMENT:
            #Check if OOB
            if pointer == 0:
                output = BrainfuckErrors.P_OUT_OF_BOUND.value
                break
            else:
                pointer -= 1
            continue
        if instruction == Instructions.C_INCREMENT:
            #Check value of stack before
            if stack[pointer] == 255:
                output = BrainfuckErrors.INCORRECT_VALUE.value
                break
            else:
                stack[pointer] += 1
            continue
        if instruction == Instructions.C_DECREMENT:
            #Check value stack first
            if stack[pointer] == 0:
                output = BrainfuckErrors.INCORRECT_VALUE.value
                break
            else:
                stack[pointer] -= 1
            continue
        if instruction == Instructions.C_OUTPUT:
            output += chr(stack[pointer])
            continue
        if instruction == Instructions.C_STORE:
            stack[pointer] = inputs[inputs_counter]
            inputs_counter += 1 
            continue
        if instruction == Instructions.JUMP_START:
            if stack[pointer] == 0:
                pc = labels[pc]
            continue
        if instruction == Instructions.JUMP_END:
            if stack[pointer] != 0:
                pc = labels[pc]
            continue

print(output)