import sys
import math
import numpy as np

# Save humans, destroy zombies!


#player = (x, y)
#humans = [(x,y)]
#zombies = ([zombie_id, zombie_x, zombie_y, zombie_xnext, zombie_ynext])
def next_zombie_pos(player, humans, zombie):
    _, zombie_x, zombie_y = zombie
    humans_with_player = [*humans, (-1,*player)]
    dist_to_human = [(x-zombie_x)**2+(y-zombie_y)**2 for _,x,y in humans_with_player]
    min_dist = min(dist_to_human)
    closest_human_id, closest_human_x, closest_human_y = humans_with_player[dist_to_human.index(min_dist)]

    min_dist = np.sqrt(min_dist)

    if min_dist <= 400:
        return closest_human_x, closest_human_y, closest_human_id
    else:
        return int(400 * (closest_human_x - zombie_x) / (min_dist)), int(400 * (closest_human_y - zombie_y) / (min_dist)), None

#player = (x, y)
#humans = [(id,x,y)]
#zombies = ([zombie_id, zombie_x, zombie_y, zombie_xnext, zombie_ynext])
#move (req_x, req_y)
#Return player (x,y), humans [(x,y)]
def simulate_next_step(player, humans, zombies, move):

    player_x, player_y = player
    move_x, move_y = move
    print(f"Player_pos: {player_x}, {player_y} asked to move to: {move_x}, {move_y}", file=sys.stderr, flush=True)
    dist_player_move = np.sqrt((player_x-move_x)**2 + (player_y - move_y)**2)
    if dist_player_move <= 1000:
        player_x = move_x
        player_y = move_y
    else:
        player_x += int(1000*(move_x-player_x)/dist_player_move)
        player_y += int(1000*(move_y-player_y)/dist_player_move)

    alive_zombies = [(idz,x,y,x_n,y_n) for idz, x, y,x_n, y_n in zombies if np.sqrt((player_x-x_n)**2+(player_y-y_n)**2) >= 2000]
    print(alive_zombies, f"Player_pos at end simulated: {player_x}, {player_y}", file=sys.stderr, flush=True)



# game loop

while True:
    x, y = [int(i) for i in input().split()]
    human_count = int(input())
    humans = [[int(j) for j in input().split()] for i in range(human_count)]
        #human_id, human_x, human_y = [int(j) for j in input().split()]
    zombie_count = int(input())
    zombies = np.array([[int(j) for j in input().split()] for i in range(zombie_count)])
    #    zombie_id, zombie_x, zombie_y, zombie_xnext, zombie_ynext = 

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    
    simulate_next_step((x, y), humans, zombies, (zombies[0][3], zombies[0][4]))
    # Your destination coordinates
    print(zombies[0][3],zombies[0][4])
    